provider "aws" {
    access_key = "xxxxx"
    secret_key = "xxxx"
    region = "ap-southeast-1"
}

/*resource "aws_instance" "nginx_web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}*/
/*resource "aws_key_pair" "aws-key" {
  key_name   = "aws-key"
  public_key = file("C:\instance-keypair\public_key.pub")// Path is in the variables file
}*/
resource "aws_instance" "nginx-web" {
  ami           = "ami-0eeadc4ab092fef70" # Use a suitable AMI ID for your region and OS
  instance_type = "t2.micro" # Choose your desired instance type
  subnet_id = aws_subnet.private_subnet.id
  tags = {
    Name = "nginx-web"
  }

  /*key_name = "aws_key_pair.aws-key.id" # Replace with your SSH key pair name
  user_data = <<-EOF
              #!/bin/bash
              sudo yum update
              sudo yum install -y nginx
              sudo service nginx start
              EOF*/

  # You can add additional configuration here as needed, such as security groups or IAM roles.
}

# Provisionin
/*resource "aws_cloudinit_instance" "nginx-web" { 
  name = aws_instance.nginx-web.id 
  instance_id = aws_instance.nginx-web.id 
    user_data = <<-EOF 
              #!/bin/bash 
              sudo yum update
              sudo yum install -y nginx
              sudo service nginx start
              EOF 
  depends_on = [aws_instance.nginx-web]
                  }*/


/*resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
}*/

/*resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "ap-southeast-1a"  # Choose your AZ
  map_public_ip_on_launch = true
}*/

resource "aws_subnet" "private_subnet" {
  vpc_id                  = var.default_vpc
  cidr_block              = "172.31.50.0/24"
  #availability_zone       = "ap-southeast-1"  # Choose your AZ
}

resource "aws_route_table" "private_rt_table" {
  vpc_id = var.default_vpc
  tags = {
    Name = "private route table"
  }
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private_rt_table.id
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = var.default_vpc

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    #cidr_blocks      = [aws_vpc.main.cidr_block]
    #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    #cidr_blocks      = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_http"
  }
}

resource "aws_security_group_rule" "allow_http_rule" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  #ipv6_cidr_blocks  = [aws_vpc.example.ipv6_cidr_block]
  security_group_id = aws_security_group.allow_http.id
}
/*
resource "aws_security_group" "nginx_sg" {
  name        = "nginx-sg"
  description = "Security group for Nginx"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_iam_role" "ec2_role" {
  name = "ec2-s3-access"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}
/*
resource "aws_iam_policy_attachment" "s3_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
  role       = aws_iam_role.ec2_role.name
} 
*/
resource "aws_launch_configuration" "nginx-web" {
  name_prefix                 = "nginx-web"
  image_id                    = "ami-0eeadc4ab092fef70"  # Choose your AMI
  instance_type               = "t2.micro"
 }

/*resource "aws_autoscaling_group" "nginx_asg" {
  name_prefix                 = "nginx-asg-"
  max_size                    = 3
  min_size                    = 3
  desired_capacity            = 3
  launch_configuration        = aws_launch_configuration.nginx-web.name
  vpc_zone_identifier         = [aws_subnet.private_subnet.id]
  target_group_arns           = [aws_lb_target_group.nginx_target_group.arn]
}*/
resource "aws_lb" "nginx_alb" {
  name               = "nginx-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = ["subnet-08a45790c48c5cbea","subnet-037e567febdab807a"]
}

resource "aws_lb_target_group" "nginx_target_group" {
  name     = "nginx-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.default_vpc
}
resource "aws_lb_listener" "nginx_listener" {
  load_balancer_arn = aws_lb.nginx_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      status_code  = "200"
      message_body = "OK"
    }
  }
}
resource "aws_s3_bucket" "nginx-web" {
  bucket = "nginx-web567"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
 }
/*terraform {
  backend "s3" {
    bucket = "S3-nginx"
    key    = "terraform.tfstate"
    region = "ap-southeast-1"
  }
}*/
